package com.professordolphin.echo.app.Utility;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.parse.ParseUser;
import com.professordolphin.echo.app.Profile.Profile;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Searching.Search;

/**
 * Handles the setup of the navigation sidebar.
 */
public class Navi {

    private static ArrayAdapter list;

    /**
     * Populates the list of navigation options for the navigation sidebar.
     *
     * @param context - the context where the navi sidebar is being set up.
     * @return - an ArrayAdapter for displaying the navi items.
     */
    public static ArrayAdapter populateNavi(Context context) {
        if (list != null) {
            return list;
        } else {
            String[] items = {"My Profile", "User Search", "Tutor Search"};
            if (ParseUser.getCurrentUser().get("type").equals("STUDENT")) {
                items[2] = "Availability";
            }
            list = new ArrayAdapter<String>(context, R.layout.navi_layout, items);
            return list;
        }
    }

    /**
     * Sets up the navigation sidebar with a listener for functionality and executes popularNavi.
     *
     * @param currentContext - the context where the navi sidebar is being set up.
     * @param navi           - the ListView that we're setting up the navi in.
     */
    public static void setUpNavi(Context currentContext, ListView navi) {
        final Context context = currentContext;

        navi.setAdapter(populateNavi(context));

        navi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = null;
                switch (position) {
                    case 0:
                        intent = new Intent(context, Profile.class);
                        break;
                    case 1:
                        break;
                    case 2:
                        intent = new Intent(context, Search.class);
                        break;
                }
                if (intent != null) {
                    context.startActivity(intent);
                }
            }
        });
    }
}
