package com.professordolphin.echo.app.Searching;

import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.professordolphin.echo.app.Notifications.PopupHandler;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Utility.Navi;

/**
 * Handles preparing searches for tutors and allows tutors to declare themselves available to tutor.
 */
public class Search extends AppCompatActivity {

    LocationManager locManager;
    Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_activity_search);
        setContentView(R.layout.activity_search);

        //set up navigation sidebar
        Navi.setUpNavi(this,(ListView) findViewById(R.id.navList));

        Spinner dropdown = (Spinner) findViewById(R.id.subject);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.subjects, android.R.layout.simple_spinner_item);
        dropdown.setAdapter(adapter);

        locManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        loc = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    }

    /**
     * Sets the subject and sends it to the Results class to handle the search. If the user is a tutor, redirects
     * to handle declaring availability.
     *
     * @param view
     */

    public void setSubject(View view) {
        if (ParseUser.getCurrentUser().get("type").equals("STUDENT")) {
            Intent results = new Intent(this, Results.class);
            results.putExtra("SUBJECT", ((Spinner) findViewById(R.id.subject)).getSelectedItem().toString());
            startActivity(results);
        } else { //user is a tutor that wants to declare availability
            declareAvailable();
        }
    }

    ParseObject available;
    ParseGeoPoint location;

    /**
     * Declares a tutor is available for tutoring services through sending an object to Parse.
     */
    public void declareAvailable() {
        available = new ParseObject("AvailableTutors");
        available.put("SUBJECT", ((Spinner) findViewById(R.id.subject)).getSelectedItem().toString());

        //put the actual ParseUser of the tutor into the ParseObject
        available.put("TUTOR", ParseUser.getCurrentUser());
        available.put("INSTALLATION", ParseInstallation.getCurrentInstallation());
        location = new ParseGeoPoint(loc.getLatitude(), loc.getLongitude());
        available.put("LOCATION", location);
        available.put("timeAway", 0);
        //send the ParseObject to the Parse servers
        available.saveInBackground();

        PopupHandler.dialogPopup(this, "Please Wait", "Now we wait for results");
        //TODO: issue11 - create way of expiring these objects
    }
}
