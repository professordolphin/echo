package com.professordolphin.echo.app.Notifications;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Handles popup messages. This class essentially works like a factory, allowing popups to be created
 * easily without having to replicate a lot of code.
 */
public final class PopupHandler {
    /**
     * Creates a popup message with a single "ok" button.
     * Useful for notifying users about occurrences that do not require any action on their part.
     *
     * @param context - the current application context.
     * @param title   - the title to display in the header of the popup.
     * @param message - the text to display in the body of the popup.
     */
    public static void dialogPopup(Context context, String title, String message) {
        new AlertDialog.Builder(context)
                .setTitle(title)
                .setMessage(message)
                .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
