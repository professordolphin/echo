package com.professordolphin.echo.app.LoginAndSignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.professordolphin.echo.app.Notifications.PopupHandler;
import com.professordolphin.echo.app.Profile.Profile;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Utility.Navi;

/**
 * Allows the user to sign up for a new profile to use the service.
 */
public class Signup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        //set up navigation sidebar
        Navi.setUpNavi(this,(ListView) findViewById(R.id.navList));
    }

    /**
     * Checks that all the fields have valid entries and creates a new ParseUser.
     *
     * @param view - The current view of the activity.
     */
    public void signup(View view) {
        //indicates if the provided field data is valid for the creation of a new user
        boolean valid = true;

        String usr = "";
        String pwd = "";
        String eml = "";
        String phn = "";
        String type = "";

        //check to make sure that every field has an entry
        try {
            usr = ((EditText) findViewById(R.id.username_field)).getText().toString();
            pwd = ((EditText) findViewById(R.id.password_field)).getText().toString();
            eml = ((EditText) findViewById(R.id.email_field)).getText().toString();
            phn = ((EditText) findViewById(R.id.phone_field)).getText().toString();

            if (((RadioButton) findViewById(R.id.tutor_button)).isChecked())
                type = "TUTOR";
            else if (((RadioButton) findViewById(R.id.student_button)).isChecked())
                type = "STUDENT";

            if (usr.isEmpty() || pwd.isEmpty() || eml.isEmpty() || phn.isEmpty() || type.isEmpty())
                //throws null pointer to handle any blanks using the same catch statement
                throw new NullPointerException("Left fields blank");
        } catch (NullPointerException npe) {
            PopupHandler.dialogPopup(this, "Try Again", "All fields are required.");
            valid = false;
        }

        //check that passwords match
        if (!pwd.equals(((EditText) findViewById(R.id.confirm_password)).getText().toString())) {
            PopupHandler.dialogPopup(this, "Password Mismatch", "Passwords must match.");
            valid = false;
        }

        //only create user if input is valid
        if (valid == true) {
            findViewById(R.id.progress).setVisibility(View.VISIBLE);
            findViewById(R.id.signup_button).setVisibility(View.INVISIBLE);

            ParseUser user = new ParseUser();
            user.setEmail(eml);
            user.setUsername(usr);
            user.setPassword(pwd);
            user.put("phone", phn);
            user.put("type", type);
            user.put("RATING",0);
            user.put("NUM_RATINGS",0);

            //send new user to Parse
            user.signUpInBackground(new SignUpCallback() {
                public void done(ParseException pe) {
                    if (pe == null) {
                        findViewById(R.id.progress).setVisibility(View.INVISIBLE);
                        findViewById(R.id.signup_button).setVisibility(View.VISIBLE);
                        // Save the current Installation to Parse, for push notifications
                        ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
                        currentInstallation.put("user", ParseUser.getCurrentUser());
                        currentInstallation.saveInBackground();

                        showProfile();
                    } else {//handle potential errors
                        findViewById(R.id.progress).setVisibility(View.INVISIBLE);
                        findViewById(R.id.signup_button).setVisibility(View.VISIBLE);
                        signupError(pe.getCode());
                    }
                }
            });
        }
    }

    /**
     * Helper method to handle error codes given by Parse during signup.
     *
     * @param code - ParseException error code
     */
    public void signupError(int code) {
        switch (code) {
            case 125:
                //invalid email address
                PopupHandler.dialogPopup(this, "Invalid Email", "Please enter a valid Email address.");
                break;
            case 202:
                //username taken
                PopupHandler.dialogPopup(this, "Username Taken", "Please try a different username.");
                break;
            case 203:
                //email taken
                PopupHandler.dialogPopup(this, "Email Taken", "Please enter a different Email address.");
                break;
            default:
                PopupHandler.dialogPopup(this, "Internal Error " + code, "Please try again later");
        }
    }

    /**
     * Displays the profile for the newly created user.
     */
    public void showProfile() {
        startActivity(new Intent(this, Profile.class));
    }
}
