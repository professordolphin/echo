package com.professordolphin.echo.app.Searching;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.professordolphin.echo.app.ImageLoading.ImageLoader;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Utility.Navi;

import java.util.ArrayList;
import java.util.List;

/**
 * Queries a search for available tutors to Parse and displays the results of the search, allowing the user
 * to view information about what tutors are available in their selected subject. The user can then select a
 * tutor to request a session.
 */
public class Results extends AppCompatActivity {

    //Lists to store results and use in multiple functions
    private ArrayList<ParseObject> tutors = new ArrayList<ParseObject>(); //List of Tutors
    private ArrayList<String> tutorNames = new ArrayList<String>();  //List of tutor names
    private ArrayList<String> tutorSubjects = new ArrayList<String>();  //List of Subjects tutor teaches
    private ArrayList<String> tutorRatings = new ArrayList<String>();  //List fo tutor ratings
    private ArrayList<String> tutorPics = new ArrayList<String>();  //List of Profile Pics

    //Location variables
    LocationManager locManager;
    Location loc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_activity_results);
        setContentView(R.layout.activity_results);

        //set up navigation sidebar
        Navi.setUpNavi(this,(ListView) findViewById(R.id.navList));

        findViewById(R.id.progress).setVisibility(View.VISIBLE); //show loading spinner

        //get the subject selected from the search screen
        Intent intent = getIntent();
        TextView searchResults = (TextView) findViewById(R.id.results_message);
        searchResults.setText("\nYour search for: " + intent.getStringExtra("SUBJECT"));

        //get location
        locManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        loc = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        //query Parse for available tutors
        ParseQuery<ParseObject> tutors = ParseQuery.getQuery("AvailableTutors");
        ParseGeoPoint currentLocation = new ParseGeoPoint(loc.getLatitude(), loc.getLongitude());
        tutors.whereWithinMiles("LOCATION", currentLocation, 10).findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(objects == null){
                    return;
                }
                //add the results to the list
                for (ParseObject result : objects) {
                    //make sure to fetch the ParseObjects from Parse, it won't do it unless this is called
                    result.fetchInBackground(new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject object, ParseException e) {
                            //needs to be handled externally to reference "this"
                            addToResults(object);
                        }
                    });
                }
            }
        });
    }

    /**
     * Adds the results from the Parse query into the results lists defined at the start of this class.
     *
     * @param result - A result from the Parse Query.
     */
    public void addToResults(ParseObject result) {
        try {
            //select the tutors that match the subject we selected
            if (getIntent().getStringExtra("SUBJECT").equals(result.get("SUBJECT"))) {
                ParseUser tempTutor = (ParseUser) result.get("TUTOR");

                //add the ParseUser to a list of ParseUsers
                tutors.add(result);

                //add the name of the ParseUser into a list of Strings that we can display
                //it's index will correspond to that in the ParseUser list "tutors"
                //the same is then done for the subject, rating and profile picture
                //of the user
                tutorNames.add(tempTutor.fetchIfNeeded().getUsername());
                tutorSubjects.add((String) result.get("SUBJECT"));

                if(((Double) ((ParseUser) result.get("TUTOR")).get("RATING") != null)){
                    tutorRatings.add(((Double) ((ParseUser) result.get("TUTOR")).get("RATING")).toString());
                }

                if(((ParseFile)((ParseUser) result.get("TUTOR")).get("PROFILE_IMAGE")) != null){
                    tutorPics.add(((ParseFile)((ParseUser) result.get("TUTOR")).get("PROFILE_IMAGE")).getUrl());
                }
            }
        } catch (ParseException pe) {
            pe.printStackTrace();
        }

        //set resultsList to reference our results_list xml file
        ListView resultsList = (ListView) findViewById(R.id.results_list);

        //make a custom Adapter using the ArrayList of username Strings "tutorNames", url Strings
        //"tutorPics", and rating strings "tutorRatings"
        OurListAdapter adapter = new OurListAdapter(this,intoArray(tutorNames),intoArray(tutorPics),intoArray(tutorRatings));

        //set up the click listener on the ListView to allow the user to interact with the list
        resultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //pass the position (the index of the tutor) that the user selected
                viewTutorInfo(position);
            }
        });


        //set the adapter for the ListView
        resultsList.setAdapter(adapter);
        findViewById(R.id.progress).setVisibility(View.INVISIBLE); //remove loading spinner
    }

    /*
    This helper method converts any ArrayList to an array
    of Strings. I used it to convert an list of doubles
    to Strings. Without this method a weird cast error occured,
    this is what motivated its creation.
    */
    private String [] intoArray(ArrayList A){
        int size = A.size();
        String [] s = new String[size];

        int i = 0;
        for(Object x: A){
            s[i] = x.toString();
            i++;
        }

        return s;
    }

    /**
     * Allows a user to select a tutor and interact with the ParseUser.
     *
     * @param position - the selected tutor, in the form of its list position.
     */
    public void viewTutorInfo(int position) {
        final ParseUser tutor = (ParseUser) tutors.get(position).get("TUTOR");
        new AlertDialog.Builder(this)
                .setTitle("Selection")
                .setMessage("Select "
                        + tutor.getUsername() + " as your tutor?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // WRONG WAY TO SEND PUSH - INSECURE!
                        //TODO: issue19 - use more secure way of push notifications
                        //see http://blog.parse.com/learn/engineering/the-dangerous-world-of-client-push/

                        ParseQuery pushQuery = ParseInstallation.getQuery();
                        pushQuery.whereEqualTo("user", tutor);
                        String message = ParseUser.getCurrentUser().getUsername() + " wants to request a session!";
                        message += "\n Contact them at: " + ParseUser.getCurrentUser().get("phone");

                        ParsePush push = new ParsePush();
                        push.setQuery(pushQuery); // Set our Installation query
                        push.setMessage(message);
                        /*JSONObject student = new JSONObject();
                        try {
                            student.put("student", ParseUser.getCurrentUser().getUsername());
                        } catch (JSONException jse) {
                            jse.printStackTrace();
                        }
                        push.setData(student);*/
                        push.sendInBackground();
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    class OurListAdapter extends ArrayAdapter{
        Context context; //context is passed in
        String [] Images; //list of user profile pics
        String [] Names; //list of usernames
        String [] Ratings; //list of tutor ratings

        public OurListAdapter(Context c, String[] Names, String images[], String [] Ratings){
            super(c,R.layout.single_row, R.id.tutorName,Names); // call superclass' constructor

            //set all the class variables
            this.context = c;
            this.Images = images;
            this.Names = Names;
            this.Ratings = Ratings;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //necessary setup
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View row = inflater.inflate(R.layout.single_row, parent, false);

            ImageView Image = (ImageView) row.findViewById(R.id.profilePic); //get ImageView in our row
            TextView Name = (TextView) row.findViewById(R.id.tutorName); //get first TextView in our row
            TextView Rating = (TextView) row.findViewById(R.id.rating); //get second TextView in our row

            /*
            This is one of the only places the "LazyList" library is used.
            The ImageLoader class extensively simplifies the code needed
            to pull a profile image from parse and display it locally.
             */
            ImageLoader loader = new ImageLoader(Results.this); //create new loader in this context
            if (Images != null && Images.length != 0 && Images[position] != null) {
                loader.DisplayImage(Images[position], Image); //pull image from url at Images[position] and display it
            }

            if(Names != null && Names.length != 0 && Names[position] != null) {
                Name.setText(Names[position]);  //display tutor's name
            }

            if(Ratings != null && Ratings.length != 0 && Ratings[position] != null) {
                Rating.setText(Ratings[position]);  //display tutor's rating
            }
            return super.getView(position,convertView,parent);
        }

    }
}


