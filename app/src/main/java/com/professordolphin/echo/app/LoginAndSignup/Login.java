package com.professordolphin.echo.app.LoginAndSignup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.professordolphin.echo.app.Notifications.PopupHandler;
import com.professordolphin.echo.app.Profile.Profile;
import com.professordolphin.echo.app.R;

/**
 * A login screen which allows the user to login using their credentials or sign up for the service.
 * Can be circumnavigated if the user elects to have the application remember their profile and remain
 * signed in.
 */
public class Login extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            if (ParseUser.getCurrentUser() != null) startActivity(new Intent(this, Profile.class));
        } catch (NullPointerException npe) {//handle quietly
        }
        super.onCreate(savedInstanceState);
        setTitle(R.string.title_activity_login);
        setContentView(R.layout.activity_login);
    }

    /**
     * Attempts to login to the service using the user-provided credentials.
     *
     * @param view - the current view.
     **/
    public void login(View view) {
        findViewById(R.id.progress).setVisibility(View.VISIBLE);
        findViewById(R.id.login_button).setVisibility(View.INVISIBLE);
        findViewById(R.id.signup_button).setVisibility(View.INVISIBLE);

        String usr = ((EditText) findViewById(R.id.username_field)).getText().toString();
        String pwd = ((EditText) findViewById(R.id.password_field)).getText().toString();

        //send auth info to Parse for authentication
        ParseUser.logInInBackground(usr, pwd, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException pe) {
                if (pe == null) {
                    // Save the current Installation to Parse, for push notifications
                    ParseInstallation currentInstallation = ParseInstallation.getCurrentInstallation();
                    currentInstallation.put("user", ParseUser.getCurrentUser());
                    currentInstallation.saveInBackground();

                    findViewById(R.id.progress).setVisibility(View.INVISIBLE);
                    findViewById(R.id.login_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.signup_button).setVisibility(View.VISIBLE);
                    showProfile();
                } else {
                    findViewById(R.id.progress).setVisibility(View.INVISIBLE);
                    findViewById(R.id.login_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.signup_button).setVisibility(View.VISIBLE);
                    loginError(pe.getCode());
                }
            }
        });
    }

    /**
     * Helper function that handles error codes for Parse login errors.
     *
     * @param errorCode - ParseException error code
     */
    public void loginError(int errorCode) {
        switch (errorCode) {
            case 101:
                //Object not found
                PopupHandler.dialogPopup(this, "Invalid Login", "Please try again.");
                break;
            default:
                PopupHandler.dialogPopup(this, "Internal Error", "Please try again later");
        }
    }

    /**
     * Displays the user's profile upon successful login or signup.
     */
    public void showProfile() {
        startActivity(new Intent(this, Profile.class));
    }

    /**
     * Send the user to the signup screen.
     *
     * @param view - the current view.
     */
    public void signup(View view) {
        startActivity(new Intent(this, Signup.class));
    }

}