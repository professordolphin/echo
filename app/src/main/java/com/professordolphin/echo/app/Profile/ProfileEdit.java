package com.professordolphin.echo.app.Profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import com.parse.DeleteCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.professordolphin.echo.app.LoginAndSignup.Login;
import com.professordolphin.echo.app.Notifications.PopupHandler;
import com.professordolphin.echo.app.Profile.Profile;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Utility.Navi;

import java.util.ArrayList;

/**
 * Allows the user to edit and perform tasks involving their profile. The user can edit certain
 * data fields in their profile, logout, and delete.
 */
public class ProfileEdit extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);

        //set up navigation sidebar
        Navi.setUpNavi(this, (ListView) findViewById(R.id.navList));
    }

    /**
     * Checks the fields for valid entries and updates a ParseUser.
     *
     * @param view - The current view of the activity.
     */
    public void editProfile(View view) {
        ParseUser user = ParseUser.getCurrentUser();
        //indicates if the provided field data is valid for the creation of a new user
        boolean valid = true;
        boolean updated = false;

        String usr = "";
        String pwd = "";
        String eml = "";
        String phn = "";

        //check to make sure that every field has an entry
        try {
            usr = ((EditText) findViewById(R.id.username_field)).getText().toString();
            pwd = ((EditText) findViewById(R.id.password_field)).getText().toString();
            eml = ((EditText) findViewById(R.id.email_field)).getText().toString();
            phn = ((EditText) findViewById(R.id.phone_field)).getText().toString();
        } catch (NullPointerException npe) {
            PopupHandler.dialogPopup(this, "Try Again", "All fields are required.");
            valid = false;
        }

        //update username if something was entered
        if (!usr.isEmpty()) {
            user.setUsername(usr);
            updated = true;
        }

        //update password if something was entered
        if (!pwd.isEmpty()) {
            //check that passwords match
            if (!pwd.equals(((EditText) findViewById(R.id.confirm_password)).getText().toString())) {
                PopupHandler.dialogPopup(this, "Password Mismatch", "Passwords must match.");
                valid = false;
            } else {
                user.setPassword(pwd);
                updated = true;
            }
        }

        //update email if something was entered
        if (!eml.isEmpty()) {
            user.setEmail(eml);
            updated = true;
        }

        //update phone number if something was entered
        if (!phn.isEmpty()) {
            user.put("phone", phn);
            updated = true;
        }

        //only update user if input is valid
        if (valid == true && updated == true) {
            //show loading spinner
            findViewById(R.id.progress).setVisibility(View.VISIBLE);
            findViewById(R.id.update_button).setVisibility(View.INVISIBLE);
            //save changes
            user.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        refreshProfile();
                    } else {
                        editProfileError(e.getCode());
                    }
                }
            });
        }
    }

    /**
     * Helper method to handle error codes given by Parse during signup.
     *
     * @param code - ParseException error code
     */
    public void editProfileError(int code) {
        findViewById(R.id.update_button).setVisibility(View.VISIBLE);
        findViewById(R.id.progress).setVisibility(View.INVISIBLE);

        switch (code) {
            case 125:
                //invalid email address
                PopupHandler.dialogPopup(this, "Invalid Email", "Please enter a valid Email address.");
                break;
            case 202:
                //username taken
                PopupHandler.dialogPopup(this, "Username Taken", "Please try a different username.");
                break;
            case 203:
                //email taken
                PopupHandler.dialogPopup(this, "Email Taken", "Please enter a different Email address.");
                break;
            default:
                PopupHandler.dialogPopup(this, "Internal Error " + code, "Please try again later");
        }
    }

    /**
     * Displays the profile for the newly updated user.
     */
    public void refreshProfile() {
        startActivity(new Intent(this, Profile.class));
    }

    /**
     * Asks the user to verify their decision and then handles requesting the user be deleted
     * from Parse.
     *
     * @param view - the current view
     */
    public void deleteProfile(View view) {
        final View thisView = view;
        new AlertDialog.Builder(this)
                .setTitle("Are you sure?")
                .setMessage("Are you sure you wish to delete your profile? It cannot be undone!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ArrayList<ParseUser> thisUser = new ArrayList<ParseUser>(1);
                        thisUser.add(ParseUser.getCurrentUser());
                        ParseUser.deleteAllInBackground(thisUser, new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                deleteProfileSuccess(thisView);
                            }
                        });
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    /**
     * External handling of a successful deletion of a profile. Tells the user the delete was successful.
     *
     * @param view
     */
    public void deleteProfileSuccess(View view) {
        PopupHandler.dialogPopup(this, "Success", "Profile deleted successfully");
        logout(view);
    }

    /**
     * Allows the user to logout of their profile and returns the user to the login screen.
     *
     * @param view
     */
    public void logout(View view) {
        ParseUser.logOutInBackground(new LogOutCallback() {
            @Override
            public void done(ParseException e) {
                logoutSuccessful();
            }
        });
    }

    /**
     * Alerts the user that a logout was successful.
     */
    public void logoutSuccessful() {
        startActivity(new Intent(this, Login.class));
    }

}
