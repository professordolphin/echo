package com.professordolphin.echo.app.Profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import com.parse.ParseUser;
import com.professordolphin.echo.app.R;
import com.professordolphin.echo.app.Searching.Search;
import com.professordolphin.echo.app.Utility.Navi;


public class Profile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        //set up navigation sidebar
        Navi.setUpNavi(this,(ListView) findViewById(R.id.navList));

        setTitle(R.string.title_activity_profile);

        //Intent intent = getIntent();
        TextView profileInfo = (TextView) findViewById(R.id.user_name);
        Button searchButton = (Button) findViewById(R.id.start_search);
        Button availableButton = (Button) findViewById(R.id.available);

        if (ParseUser.getCurrentUser().get("type").equals("TUTOR")) {
            availableButton.setVisibility(View.VISIBLE);
            searchButton.setVisibility(View.INVISIBLE);
        } else {
            availableButton.setVisibility(View.INVISIBLE);
            searchButton.setVisibility(View.VISIBLE);
        }

        // Display user values for the appropriate fields
        displayUserData();
    }

    /**
     * Goes to the search screen to create a search of availability request to Parse.
     *
     * @param view - the current view.
     */
    public void startSearch(View view) {
        Intent search = new Intent(this, Search.class);
        startActivity(search);
    }

    /**
     * Allows the user to edit aspects of their profile.
     *
     * @param view - the current view.
     */
    public void editProfile(View view) {
        startActivity(new Intent(this, ProfileEdit.class));
    }

    /**
     * Pulls the appropriate user information from Parse and sets
     * it in the correct fields.  Called initially from onCreate.
     */
    private void displayUserData() {
        // Get display components by resource ID for editing
        TextView profileInfo = (TextView) findViewById(R.id.user_name);
        TextView profileType = (TextView) findViewById(R.id.profile_type);
        TextView profileEmail = (TextView) findViewById(R.id.profile_email);
        Button searchButton = (Button) findViewById(R.id.start_search);
        Button availableButton = (Button) findViewById(R.id.available);

        // Display the user's name
        String userName = (String) ParseUser.getCurrentUser().getUsername();
        profileInfo.setText(userName);

        // Display the user's account type, tutor or student
        if (ParseUser.getCurrentUser().get("type").equals("TUTOR")) {
            profileType.setText(R.string.profile_type_tutor);
        } else {
            profileType.setText(R.string.profile_type_student);
        }

        // Display the user's email address
        String email = (String) ParseUser.getCurrentUser().getEmail();
        profileEmail.setText(email);
    }
}
