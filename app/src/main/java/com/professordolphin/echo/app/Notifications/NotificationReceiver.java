package com.professordolphin.echo.app.Notifications;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.parse.ParsePushBroadcastReceiver;
import com.professordolphin.echo.app.Profile.Profile;

/**
 * Overrides the default Parse notification handler to allow us to determine characteristics of the
 * push notifications such as appearance and responses to user interactions.
 */
public class NotificationReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected Class<? extends Activity> getActivity(Context context, Intent intent) {
        //TODO: decide what to do when notification is selected
        Intent launchIntent = new Intent(context, Profile.class);
        String className = launchIntent.getComponent().getClassName();
        Class<? extends Activity> cls = null;
        try {
            cls = (Class<? extends Activity>) Class.forName(className);
        } catch (ClassNotFoundException e) {
            // do nothing
        }
        return cls;
    }
}
