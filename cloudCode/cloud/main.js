Parse.Cloud.job("tutorCleanup", function(request, status) {
  Parse.Cloud.useMasterKey();
  //First query: Finding tutors to delete (30 minutes has passed and they have not continued their availability)
  var deleted = 0; //Number of deleted tutors from AvailableTutors
  var removeQuery = new Parse.Query("AvailableTutors");
  removeQuery.greaterThanOrEqualTo("timeAway", 30);
  removeQuery.each(function(tutor){
      //Setting the isAvailable field in Users to be false for each tutor
      var userQuery = new Parse.Query("Users");
      userQuery.equalTo("objectId", tutor.get("objectId"));
      userQuery.each(function(user){
        user.set("isAvailable", false);
      })
      tutor.destroy(); //Actually removes the tutor
      deleted++;
      return tutor.save();
  });
  //Second query: Finding tutors to notify
  var notifyQuery = new Parse.Query("AvailableTutors");
  var notified = 0; //Number of notified tutors
  notifyQuery.greaterThanOrEqualTo("timeAway", 25);
  notifyQuery.lessThan("timeAway", 30);
  notifyQuery.each(function(tutor){
  	notified++;
    //Sending the notification
    Parse.Push.send({
  	  where: tutor.get("INSTALLATION"), // Set our Installation query
  	  data: {
  	    alert: "Are you still available?"
  	  }
  	  }, {
  	  success: function() {
  	    console.log("Successful push");
  	  },
  	  error: function(error) {
  	    console.error("Error occurred in Push");
  	  }
  	  });
    });
  //Third query: Incrementing the timeAway field for all available tutors
  var allQuery = new Parse.Query("AvailableTutors");
  var incremented = 0; //Number of tutors who have the timeAway incremented
  allQuery.greaterThanOrEqualTo("timeAway", 0);
  allQuery.each(function(tutor){
    incremented++;
    tutor.increment("timeAway", 5);
    return tutor.save();
  }).then(function(){
    status.success("Reached end of job with these stats\nDeleted: " + deleted + "\nNotified: " + notified + "\nTime incremented: " + incremented);
  }, function(noResults) {
    status.success("Reached end of job with these stats\nDeleted: " + deleted + "\nNotified: " + notified + "\nTime incremented: " + incremented);
  });
});